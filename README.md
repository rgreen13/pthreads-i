### Calculating Pi with pThreads ###

A basic example showing how to calculate pi and return values from a pThreads function. Note that dynamically allocated and returned memory must be deallocated as well!

To compile and run from command line:
```
g++ main.cpp CStopWatch.cpp
./a.out
```
or
```
   cd Default && make all
```

To compile and run with Docker:
```
docker run --rm -v ($pwd):/tmp -w /tmp/Default rgreen13/alpine-bash-gpp make all
docker run --rm -v ($pwd):/tmp -w /tmp/Default rgreen13/alpine-bash-bpp ./pThreads_Timing
```
